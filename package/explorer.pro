TEMPLATE = app
CONFIG += debug
SOURCES = ../sources/explorer/main.cpp ../sources/explorer/desktop.cpp ../sources/explorer/taskbar.cpp ../sources/explorer/explorer.cpp
HEADERS = ../sources/explorer/desktop.h ../sources/explorer/taskbar.h ../sources/explorer/explorer.h
QT += widgets gui svg x11extras
TARGET = explorer
RESOURCES = ../sources/_files/materialdesignicons.qrc ../sources/_files/fonts.qrc
unix:LIBS += -lxcb
!exists( ../sources/explorer/main.cpp ) {
    error( "No main.cpp file found" )
}

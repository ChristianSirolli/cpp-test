#include <QStylePlugin>
#include "glassmorphism_p.h"

QT_BEGIN_NAMESPACE

class GlassMorphismPlugin : public QStylePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QStyleFactoryInterface" FILE "glassmorphism.json")
public:
    QStyle *create(const QString &key);
};

QStyle *GlassMorphismPlugin::create(const QString &key)
{
    GMAutoReleasePool pool;
    if (key.compare(QLatin1String("glassmorphism"), Qt::CaseInsensitive) == 0)
        return new GlassMorphism();

    return 0;
}

QT_END_NAMESPACE

#ifndef GLASSMORPHISM_P_P_H
#define GLASSMORPHISM_P_P_H

#include "glassmorphism_p.h"

#include <QDebug>
#include <QHash>
#include <QList>
#include <QMap>
#include <QMath>
#include <QPair>
#include <QPointer>
#include <QTextStream>

#include <QtGui/private/qpainter_p.h>

#include <QBitmap>
#include <QEvent>
#include <QPaintDevice>
#include <QPainter>
#include <QPixmapCache>

#include <QtWidgets/private/qapplication_p.h>
#include <QtWidgets/private/qcommonstyle_p.h>
#include <QtWidgets/private/qstylehelper_p.h>

#include <QApplication>
#include <QFocusFrame>
#include <QFormLayout>
#include <QLayout>
#include <QStyleOption>
#include <QTextEdit>

#if QT_CONFIG(checkbox)
#include <QCheckbox>
#endif
#if QT_CONFIG(combobox)
#include <QtWidgets/private/qcombobox_p.h>
#include <QComboBox>
#endif
#if QT_CONFIG(datetimeedit)
#include <Qdatetimeedit.h>
#endif
#if QT_CONFIG(dialogbuttonbox)
#include <Qdialogbuttonbox.h>
#endif
#if QT_CONFIG(dockwidget)
#include <Qdockwidget.h>
#endif
#if QT_CONFIG(graphicsview)
#include <Qgraphicsproxywidget.h>
#include <Qgraphicsview.h>
#endif
#if QT_CONFIG(groupbox)
#include <Qgroupbox.h>
#endif
#if QT_CONFIG(itemviews)
#include <Qheaderview.h>
#endif
#if QT_CONFIG(lineedit)
#include <Qlineedit.h>
#endif
#if QT_CONFIG(listview)
#include <Qlistview.h>
#endif
#if QT_CONFIG(mainwindow)
#include <Qmainwindow.h>
#endif
#if QT_CONFIG(menubar)
#include <Qmenubar.h>
#endif
#if QT_CONFIG(progressbar)
#include <Qprogressbar.h>
#endif
#if QT_CONFIG(pushbutton)
#include <Qpushbutton.h>
#endif
#include <Qradiobutton.h>
#if QT_CONFIG(rubberband)
#include <Qrubberband.h>
#endif
#if QT_CONFIG(sizegrip)
#include <Qsizegrip.h>
#endif
#if QT_CONFIG(spinbox)
#include <Qspinbox.h>
#endif
#if QT_CONFIG(splitter)
#include <Qsplitter.h>
#endif
#if QT_CONFIG(tableview)
#include <Qtableview.h>
#endif
#if QT_CONFIG(toolbar)
#include <Qtoolbar.h>
#endif
#if QT_CONFIG(toolbutton)
#include <Qtoolbutton.h>
#endif
#if QT_CONFIG(treeview)
#include <Qtreeview.h>
#endif

Q_FORWARD_DECLARE_MUTABLE_CG_TYPE(CGContext);

Q_FORWARD_DECLARE_OBJC_CLASS(NSView);
Q_FORWARD_DECLARE_OBJC_CLASS(NSCell);

QT_BEGIN_NAMESPACE

/*
    AHIG:
        macOS Human Interface Guidelines
        https://developer.apple.com/macos/human-interface-guidelines/overview/themes/
    Builder:
        Interface Builder in Xcode 8 or later
*/

// this works as long as we have at most 16 different control types
#define CT1(c) CT2(c, c)
#define CT2(c1, c2) ((uint(c1) << 16) | uint(c2))

#define SIZE(large, small, mini) \
    (controlSize == QStyleHelper::SizeLarge ? (large) : controlSize == QStyleHelper::SizeSmall ? (small) : (mini))

// same as return SIZE(...) but optimized
#define return_SIZE(large, small, mini) \
    do { \
        static const int sizes[] = { (large), (small), (mini) }; \
        return sizes[controlSize]; \
    } while (false)

class GlassMorphismPrivate : public QCommonStylePrivate
{
    Q_DECLARE_PUBLIC(GlassMorphism)
public:
    enum Direction {
        North, South, East, West
    };

    enum CocoaControlType {
        NoControl,    // For when there's no such a control in Cocoa
        Box,          // QGroupBox
        Box_Dark,     // FIXME See render code in drawPrimitive(PE_FrameTabWidget)
        Button_CheckBox,
        Button_Disclosure,  // Disclosure triangle, like in QTreeView
        Button_PopupButton,  // Non-editable QComboBox
        Button_PullDown, // QPushButton with menu
        Button_PushButton, // Plain QPushButton and QTabBar buttons
        Button_RadioButton,
        Button_SquareButton, // Oversized QPushButton
        Button_WindowClose,
        Button_WindowMiniaturize,
        Button_WindowZoom,
        ComboBox,     // Editable QComboBox
        ProgressIndicator_Determinate,
        ProgressIndicator_Indeterminate,
        Scroller_Horizontal,
        Scroller_Vertical,
        SegmentedControl_First, // QTabBar buttons focus ring
        SegmentedControl_Middle,
        SegmentedControl_Last,
        SegmentedControl_Single,
        Slider_Horizontal,
        Slider_Vertical,
        SplitView_Horizontal,
        SplitView_Vertical,
        Stepper,      // QSpinBox buttons
        TextField
    };

    struct CocoaControl {
        CocoaControl();
        CocoaControl(CocoaControlType t, QStyleHelper::WidgetSizePolicy s);

        CocoaControlType type;
        QStyleHelper::WidgetSizePolicy size;

        bool operator==(const CocoaControl &other) const;

        QSizeF defaultFrameSize() const;
        QRectF adjustedControlFrame(const QRectF &rect) const;
        QMarginsF titleMargins() const;

        bool getCocoaButtonTypeAndBezelStyle(NSButtonType *buttonType, NSBezelStyle *bezelStyle) const;
    };


    typedef void (^DrawRectBlock)(CGContextRef, const CGRect &);

    GlassMorphismPrivate();
    ~GlassMorphismPrivate();

    // Ideally these wouldn't exist, but since they already exist we need some accessors.
    static const int PushButtonLeftOffset;
    static const int PushButtonRightOffset;
    static const int PushButtonContentPadding;

    enum Animates { AquaPushButton, AquaProgressBar, AquaListViewItemOpen, AquaScrollBar };
    QStyleHelper::WidgetSizePolicy aquaSizeConstrain(const QStyleOption *option, const QWidget *widg,
                             QStyle::ContentsType ct = QStyle::CT_CustomBase,
                             QSize szHint=QSize(-1, -1), QSize *insz = 0) const;
    QStyleHelper::WidgetSizePolicy effectiveAquaSizeConstrain(const QStyleOption *option, const QWidget *widg,
                             QStyle::ContentsType ct = QStyle::CT_CustomBase,
                             QSize szHint=QSize(-1, -1), QSize *insz = 0) const;
    inline int animateSpeed(Animates) const { return 33; }

    // Utility functions
    static CGRect comboboxInnerBounds(const CGRect &outterBounds, const CocoaControl &cocoaWidget);

    static QRectF comboboxEditBounds(const QRectF &outterBounds, const CocoaControl &cw);

    void setAutoDefaultButton(QObject *button) const;

    NSView *cocoaControl(CocoaControl widget) const;
    NSCell *cocoaCell(CocoaControl widget) const;

    void setupNSGraphicsContext(CGContextRef cg, bool flipped) const;
    void restoreNSGraphicsContext(CGContextRef cg) const;

    void setupVerticalInvertedXform(CGContextRef cg, bool reverse, bool vertical, const CGRect &rect) const;

    void drawNSViewInRect(NSView *view, const QRectF &rect, QPainter *p, __attribute__((noescape)) DrawRectBlock drawRectBlock = nil) const;
    void resolveCurrentNSView(QWindow *window) const;

    void drawFocusRing(QPainter *p, const QRectF &targetRect, int hMargin, int vMargin, const CocoaControl &cw) const;

    void drawToolbarButtonArrow(const QStyleOption *opt, QPainter *p) const;

    QPainterPath windowPanelPath(const QRectF &r) const;

    CocoaControlType windowButtonCocoaControl(QStyle::SubControl sc) const;

#if QT_CONFIG(tabbar)
    void tabLayout(const QStyleOptionTab *opt, const QWidget *widget, QRect *textRect, QRect *iconRect) const override;
    static Direction tabDirection(QTabBar::Shape shape);
    static bool verticalTabs(GlassMorphismPrivate::Direction tabDirection);
#endif

public:
    mutable QPointer<QObject> autoDefaultButton;
    static  QList<QPointer<QObject> > scrollBars;

    mutable QPointer<QFocusFrame> focusWidget;
    mutable NSView *backingStoreNSView;
    mutable QHash<CocoaControl, NSView *> cocoaControls;
    mutable QHash<CocoaControl, NSCell *> cocoaCells;

    QFont smallSystemFont;
    QFont miniSystemFont;

    GMKeyValueObserver appearanceObserver;
};

QT_END_NAMESPACE

#endif // GLASSMORPHISM_P_P_H

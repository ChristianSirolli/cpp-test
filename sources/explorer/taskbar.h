#ifndef TASKBAR_H
#define TASKBAR_H
#include <QWidget>
#include <QSize>
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QToolButton>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QRect>
#include <QObject>
class WinTaskbar : public QObject {
    Q_OBJECT
    private:
        QWidget taskbarWidget;
        QSize tbbSize{50, 45}; // taskbar button size
        QSize tbiSize{45, 45}; // taskbar icon size
        QGridLayout *tblayout = new QGridLayout{&taskbarWidget};
        QPushButton startButton;
        QLineEdit searchBar;
        QToolButton searchIcon;
        QPushButton cortana;
        QPushButton taskViewButton;
        QGridLayout ablayout; // App list
        QSpacerItem appSpacer{0, tbbSize.height(), QSizePolicy::MinimumExpanding, QSizePolicy::Preferred};
        QGridLayout iblayout; // Notification icons
        QPushButton timedate; // Time/Date
        QPushButton notifButton; // Notification Button
        QPushButton showDesktopButton;
        QSpacerItem farLeftSpacer{12, tbbSize.height(), QSizePolicy::Fixed, QSizePolicy::Fixed};
        int screenHeight;
        int screenWidth;
    private slots:
        void updateTimeDate();
        void updateSize(const QRect &screenSize);
    public:
        WinTaskbar(QScreen *screen);
        void initStartBtn();
        void initSearchBar();
        void initCortanaBtn();
        void initTaskViewBtn();
        void initAppLayout();
        void initNotifIcons();
        void initTimeDateBtn();
        void initNotifBtn();
        void initShowDesktopBtn();
};
#endif

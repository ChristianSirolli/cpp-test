#include <iostream>
#include <string>
#include <QWidget>
#include <QResource>
#include "explorer.h"
WinExplorer::WinExplorer() {
    std::cout << "Initializing File Explorer" << std::endl;
    QResource::registerResource("/usr/lib/System32/B00merang_Artwork_Windows_10.rcc");
    window.resize(200, 150);
    // window.setWindowTitle("File Explorer");
    window.show();
}

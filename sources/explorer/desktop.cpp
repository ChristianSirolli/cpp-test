#include <iostream>
#include <string>
#include <Qt>
#include <QMainWindow>
#include <QRect>
#include <QResource>
#include "desktop.h"
// #include <QStyleOption>
// #include <QPainter>
// #include <QStyle>
// #include <QPaintEvent>

// class Desktop {
    // void CustomWidget::paintEvent(QPaintEvent *) {
    //     QStyleOption opt;
    //     opt.init(this);
    //     QPainter p(this);
    //     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    // }
// };
WinDesktop::WinDesktop(QRect *screenSize) {
    std::cout << "Initializing Desktop" << std::endl;
    QResource::registerResource("/usr/lib/System32/B00merang_Artwork_Windows_10.rcc");
    screenHeight = screenSize->height();
    screenWidth = screenSize->width();
    desktopWindow.setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint);
    desktopWindow.setAttribute(Qt::WA_X11NetWmWindowTypeDesktop, true);
    desktopWindow.setAttribute(Qt::WA_TranslucentBackground, true);
    desktopWindow.setStyleSheet("background: transparent;");
    desktopWindow.setGeometry(0, 0, screenWidth, screenHeight);
    desktopWindow.show();
}

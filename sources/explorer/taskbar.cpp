#include <iostream>
#include <string>
#include <QWidget>
#include <QApplication>
#include <Qt>
#include <QSize>
#include <QGridLayout>
#include <QPushButton>
#include <QPixmap>
#include <QLineEdit>
#include <QToolButton>
#include <QString>
#include <QStyle>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QByteArray>
#include <QRect>
#include <QTimer>
#include <ctime>
#include <QScreen>
#include <QX11Info>
#include <QResource>
// #include <X11/Xlib.h>
#include <xcb/xcb.h>
#include "taskbar.h"
void WinTaskbar::updateTimeDate() {
    time_t now = time(0);
    tm *ltm = localtime(&now);
    timedate.setText(QString("%1:%2 %3\n%4/%5/%6").arg(QByteArray::number((ltm->tm_hour > 12) ? ltm->tm_hour - 12 : (ltm->tm_hour == 0) ? 12 : ltm->tm_hour), QByteArray::number(ltm->tm_min).prepend((ltm->tm_min < 10 ? "0": "")), (ltm->tm_hour > 12) ? "PM" : "AM", QByteArray::number(1 + ltm->tm_mon), QByteArray::number(ltm->tm_mday), QByteArray::number(1900 + ltm->tm_year)));
}
void WinTaskbar::updateSize(const QRect &screenSize) {
    std::cout << "Screen size changed, updating Taskbar geometry" << std::endl;
    screenHeight = screenSize.height();
    screenWidth = screenSize.width();
    taskbarWidget.setGeometry(0, screenHeight - 45, screenWidth, 45);
}
void WinTaskbar::initStartBtn() {
    std::cout << "Initializing Start Button" << std::endl;
    // QPixmap startIcon(":places/start-here-symbolic.svg");
    QPixmap startIcon(":windows.svg");
    startButton.setIcon(startIcon);
    startButton.setFlat(true);
    startButton.setContentsMargins(0,0,0,0);
    startButton.setFixedSize(tbbSize);
    startButton.setIconSize(tbiSize);
    // startButton.setMenu();
    tblayout->addWidget(&startButton, 0, 0, Qt::AlignLeft);
}
void WinTaskbar::initSearchBar() {
    std::cout << "Initializing Search Bar" << std::endl;
    searchBar.setContentsMargins(0,0,0,0);
    // QPixmap searchIcon(":places/start-here-symbolic.svg");
    // searchBar.setIcon(searchIcon);
    // searchBar.setFrameStyle(QFrame::Panel | QFrame::Plain);
    // searchBar.setLineWidth(0);
    searchBar.setPlaceholderText("Type here to search");
    // startButton.setFlat(true);
    // searchBar.setSizePolicy(QSizePolicy::Fixed); // Apparently this is private in this context, no idea why
    searchBar.setMinimumSize(tbbSize.width() * 10, tbbSize.height()); //This doesn't change the width at all

    QPixmap searchSVG(":actions/edit-find-symbolic.svg");
    searchIcon.setIcon(searchSVG);
    searchIcon.setIconSize(tbiSize);
    searchIcon.setCursor(Qt::ArrowCursor);
    searchIcon.setStyleSheet("QToolButton { border: none; padding: 0px; }");
    int frameWidth = searchBar.style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    searchBar.setStyleSheet(QString("QLineEdit { padding-left: %1px; } ").arg(searchIcon.sizeHint().width() + frameWidth + 1));
    QSize msz = searchBar.minimumSizeHint();
    searchBar.setMinimumSize(qMax(msz.width(), searchIcon.sizeHint().height() + frameWidth * 2 + 2), qMax(msz.height(), searchIcon.sizeHint().height() + frameWidth * 2 + 2));
    
    tblayout->addWidget(&searchBar, 0, 1, Qt::AlignLeft);
}
void WinTaskbar::initCortanaBtn() {
    std::cout << "Initializing Cortana Button" << std::endl;
    cortana.setContentsMargins(0,0,0,0);
    // cortana.setIcon(QIcon(":cortana.svg").pixmap(tbiSize));
    QPixmap cortanaIcon(":cortana.svg");
    cortana.setIcon(cortanaIcon);
    cortana.setFlat(true);
    cortana.setFixedSize(tbbSize);
    cortana.setIconSize(tbiSize);
    // cortana.setMenu();
    tblayout->addWidget(&cortana, 0, 2, Qt::AlignLeft);
}
void WinTaskbar::initTaskViewBtn() {
    std::cout << "Initializing Task View Button" << std::endl;
    taskViewButton.setContentsMargins(0,0,0,0);
    // QPixmap taskViewIcon(":apps/preferences-system-notifications-symbolic.svg");
    // taskViewButton.setIcon(taskViewIcon);
    taskViewButton.setFlat(true);
    taskViewButton.setFixedSize(tbbSize);
    taskViewButton.setIconSize(tbiSize);
    // taskViewButton.setMenu();
    tblayout->addWidget(&taskViewButton, 0, 3, Qt::AlignLeft);
}
void WinTaskbar::initAppLayout() {
    std::cout << "Initializing App Layout" << std::endl;
    ablayout.setSpacing(0);
    ablayout.setContentsMargins(0,0,0,0);
    tblayout->addLayout(&ablayout, 0, 4, Qt::AlignJustify);

    ablayout.addItem(&appSpacer, 0, 0, 1, -1, Qt::AlignRight);
}
void WinTaskbar::initNotifIcons() {
    std::cout << "Initializing Notification Icons" << std::endl;
    iblayout.setSpacing(0);
    iblayout.setContentsMargins(0,0,0,0);
    tblayout->addLayout(&iblayout, 0, 5, Qt::AlignRight);
}
void WinTaskbar::initTimeDateBtn() {
    std::cout << "Initializing Time Date Button" << std::endl;
    timedate.setContentsMargins(0,0,0,0);
    timedate.setFlat(true);
    timedate.setFixedSize(tbbSize.width() * 1.25, tbbSize.height());
    timedate.setStyleSheet("text-align: center");
    tblayout->addWidget(&timedate, 0, 6, Qt::AlignRight);
    QTimer *timer = new QTimer(this);
    timedate.connect(timer, &QTimer::timeout, this, &WinTaskbar::updateTimeDate);
    // QTimer *timer = new QTimer(&timedate);
    // timedate.connect(timer, &QTimer::timeout, &timedate, &WinTaskbar::updateTimeDate);
    timer->start(1000);
    updateTimeDate();
}
void WinTaskbar::initNotifBtn() {
    std::cout << "Initializing Notification Button" << std::endl;
    notifButton.setContentsMargins(0,0,0,0);
    QPixmap notifIcon(":apps/preferences-system-notifications-symbolic.svg");
    notifButton.setIcon(notifIcon);
    notifButton.setFlat(true);
    notifButton.setFixedSize(tbbSize.width() - 8, tbbSize.height());
    notifButton.setIconSize(tbiSize);
    // notifButton.setMenu();
    tblayout->addWidget(&notifButton, 0, 7, Qt::AlignRight);
}
void WinTaskbar::initShowDesktopBtn() {
    std::cout << "Initializing Show Desktop Button" << std::endl;
    showDesktopButton.setContentsMargins(0,0,0,0);
    // QPixmap showDesktopIcon(":places/start-here-symbolic.svg");
    // showDesktopButton.setIcon(showDesktopIcon);
    showDesktopButton.setFlat(true);
    showDesktopButton.setAutoFillBackground(true);
    showDesktopButton.setStyleSheet("border-left: 1px solid white;");
    showDesktopButton.setFixedSize(6, tbbSize.height());
    tblayout->addWidget(&showDesktopButton, 0, 9, Qt::AlignRight);
}
WinTaskbar::WinTaskbar(QScreen *screen) {
    std::cout << "Initializing Taskbar" << std::endl;
    QResource::registerResource("/usr/lib/System32/B00merang_Artwork_Windows_10.rcc");
    QRect screenSize = screen->geometry();
    screenHeight = screenSize.height();
    screenWidth = screenSize.width();
    taskbarWidget.setAttribute(Qt::WA_X11NetWmWindowTypeDock, true);
    taskbarWidget.setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::CustomizeWindowHint);
    taskbarWidget.setStyleSheet("border: none; padding: 0; margin: 0; background: green");
    taskbarWidget.setGeometry(0, screenHeight - tbbSize.width(), screenWidth, tbbSize.height());
    taskbarWidget.setContentsMargins(0,0,0,0);
    taskbarWidget.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    tblayout->setRowMinimumHeight(0, tbbSize.height());
    tblayout->setSpacing(0);
    tblayout->setVerticalSpacing(0);
    tblayout->setContentsMargins(0,0,0,0);
    taskbarWidget.setLayout(tblayout);
    initStartBtn();
    initSearchBar();
    initCortanaBtn();
    initTaskViewBtn();
    initAppLayout();
    initTimeDateBtn();
    initNotifBtn();
    // initShowDesktopBtn();
    tblayout->addItem(&farLeftSpacer, 0, 8, 1, -1, Qt::AlignRight);
    taskbarWidget.show();
    connect(screen, &QScreen::geometryChanged, this, &WinTaskbar::updateSize);
}

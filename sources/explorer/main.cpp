#include <iostream>
#include <string>

#include <QApplication>
#include <QObject>
#include <QScreen>
#include <QResource>
#include <QFontDatabase>
#include <QRect>
#include <QDesktopWidget>
#include <QList>
#include <QScreen>
#include <QSharedMemory>
#include <QIcon>
#include <QStyleFactory>

#include "desktop.h"
#include "taskbar.h"
#include "explorer.h"
QSharedMemory sharedMemory("winux_explorer");
void cleanUp() {
    sharedMemory.detach();
}
int main(int argc, char **argv) {
    std::cout << "Qt version: " << qVersion() << std::endl;
    QApplication app(argc, argv);
    app.setApplicationDisplayName("winux-explorer");
    app.setDesktopFileName("explorer");
    app.setApplicationName("winux-explorer");
    app.setStyle(QStyleFactory::create("GlassMorphism"));

    QResource::registerResource("/usr/lib/System32/B00merang_Artwork_Windows_10.rcc");
    app.setWindowIcon(QIcon(":apps/system-file-manager-symbolic.svg"));

    QFontDatabase fontdb;
    fontdb.addApplicationFont(":selawk.ttf");
    fontdb.addApplicationFont(":selawkb.ttf");
    fontdb.addApplicationFont(":selawkl.ttf");
    fontdb.addApplicationFont(":selawksb.ttf");
    fontdb.addApplicationFont(":selawksl.ttf");
    /*
    explorer is a 3 part program:
    1. Desktop - shows background, app shortcuts, files. Shortcuts and files are located in ~/Desktop
    2. Taskbar - has start menu, search box, "cortana", task view, app shortcuts, system icons, a calendar, notifications and a button to peek or show the Desktop
    3. File Explorer - the file manager

    Killing the explorer process should kill all the above as well, as it is done in Windows.
    */

    // Checks if a QSharedMemory with key "winux_explorer" exists
    // If not, the desktop and taskbar aren't present, so start them
    // If so, `explorer` is running, so open File Explorer
    sharedMemory.attach();
    if (sharedMemory.create(1) == true) {
        QList<QScreen *> screens = QApplication::screens();
        QList<WinTaskbar *> taskbars;
        QList<WinDesktop *> desktops;
        int screens_size = screens.size();
        std::cout << screens_size << " screen(s) found:" << std::endl;
        for (int i = 0; i < screens_size; i++) {
            QScreen *screen = screens.at(i);
            if (screen) {
                QRect screenSize = screen->geometry();
                std::cout << "  " << i << " : " << screenSize.x() << ", " << screenSize.y() << ", " << screenSize.width() << ", " << screenSize.height() << std::endl;
                desktops << new WinDesktop(&screenSize);
                taskbars << new WinTaskbar(screen);
                std::cout << "Finished initializing Desktop and Taskbar" << std::endl;
            }
        }
        QObject::connect(&app, &QApplication::aboutToQuit, cleanUp);
    } else {
        std::cout << "An instance of `explorer` already exists, let's open Winux Explorer!" << std::endl;
        sharedMemory.detach();
        app.setApplicationDisplayName("Winux Explorer");
        QList<WinExplorer *> explorers;
        explorers << new WinExplorer();
    }
    return app.exec();
}

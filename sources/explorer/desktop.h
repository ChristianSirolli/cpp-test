#ifndef DESKTOP_H
#define DESKTOP_H
#include <QMainWindow>
#include <QRect>
class WinDesktop {
    private:
        QMainWindow desktopWindow;
        int screenHeight;
        int screenWidth;
    public:
        WinDesktop(QRect *screenSize);
};
#endif


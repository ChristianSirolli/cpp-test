TEMPLATE = app
CONFIG += debug
SOURCES = ../sources/settings/main.cpp
QT += widgets gui svg x11extras
TARGET = explorer
RESOURCES = ../sources/_files/materialdesignicons.qrc ../sources/_files/fonts.qrc
unix:LIBS += -lxcb
!exists( ../sources/settings/main.cpp ) {
    error( "No main.cpp file found" )
}


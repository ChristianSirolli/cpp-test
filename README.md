# Winux Desktop Environment
[![Build status](https://ci.appveyor.com/api/projects/status/v442ji8fg32wiig2?svg=true)](https://ci.appveyor.com/project/ChristianSirolli/cpp-test)

This repo is the WDE project. It is a Windows-like desktop environment for Linux.

## Dependencies
### XServer related
* xdm - Login/display manager
* mutter - Window manager
* plymouth
* plymouth-theme-script

### Fonts
* libchewing3
* ttf-mscorefonts-installer
* fonts-wine

### Software
* microsoft-edge-dev - Microsoft Edge Browser (dev)

### Libraries
* libqt5gui5
* winehq-devel
* wine-binfmt
* dosbox
* exe-thumbnailer
* q4wine
* network-manager
* network-manager-openvpn

## Notes
This will create Windows-like user directories in `/etc/skel/` and all users in `/home/` if those directories don't already exist.

`.xsession` will be overwritten.